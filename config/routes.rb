Rails.application.routes.draw do

  resources :items
  root 'items#index'
  
  get 'thanks_page', to: 'thanks_page#view'
  post 'thanks_page', to: 'thanks_page#view'  
  get    'admin'   => 'sessions#new'
  post   'admin'   => 'sessions#create'
  
  get 'list', to: 'sessions#list'
  post   'list', to: 'sessions#list'
end
