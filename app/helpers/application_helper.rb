module ApplicationHelper

 # ページごとの完全なタイトルを返す
  def full_title(page_title = '')
    base_title = "Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
  
  def item_image_tag item
    case item.id
    when 1
      image_tag 'daikon.jpeg'
    when 2
      image_tag 'shintama.jpeg'
    when 3
      image_tag 'ninjin.jpeg'
    when 4
      image_tag 'imo.jpeg'
    when 5
      image_tag 'buta3.jpeg'
    end
  end
end


